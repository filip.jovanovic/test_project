CREATE TABLE COMPANY_DEPARTMENTS (
	ID_DEP int,
    NAME varchar(50),
    LOCATION enum('Podgorica', 'Nikšić', 'Žabljak', 'Bar') DEFAULT 'Podgorica',
    MANAGER varchar(50),
    PRIMARY KEY (ID_DEP)
);

CREATE TABLE COMPANY_EMPLOYEES (
	ID int,
    DEP	int,
    NAME varchar(40),
    PHONE_NUMBER varchar(12),
    DATE_OF_EMPLOYMENT date,
    SALARY double,
    PRIMARY KEY (ID),
    FOREIGN KEY (DEP) REFERENCES COMPANY_DEPARTMENTS(ID_DEP)
);

INSERT INTO COMPANY_DEPARTMENTS(ID_DEP, NAME, LOCATION, MANAGER)
VALUES(1, 'IT', 'Podgorica', 'Filip Jovanovic'),
(2, 'Sales', 'Nikšić', 'Marko Markovic'),
(3, 'Marketing', 'Bar', 'Janko Jankovic');

INSERT INTO COMPANY_EMPLOYEES(ID, DEP, NAME, PHONE_NUMBER, DATE_OF_EMPLOYMENT, SALARY)
VALUES(1, 1, 'Filip Jovanovic', '069000001', '2024-01-10', 450.50),
(2, 2, 'Marko Markovic', '069000002', '2024-01-11', 750.50),
(3, 2, 'Petar Petrovic', '069000003', '2024-02-01', 950.50),
(4, 3, 'Janko Jankovic', '069000004', '2023-12-31', 450.50);
