SELECT * FROM COMPANY_EMPLOYEES
WHERE date_format(DATE_OF_EMPLOYMENT, '%d %c') = date_format(date_add(curdate(), INTERVAL 1 DAY), '%d %c');
